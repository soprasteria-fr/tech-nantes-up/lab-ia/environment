FROM python:3.10-slim

ENV OPENVSCODE_SERVER_VERSION "1.93.0"

RUN apt -yqq update && \
    apt -yqq install git vim nano wget && \
    apt clean

WORKDIR /vscode

RUN wget "https://github.com/gitpod-io/openvscode-server/releases/download/openvscode-server-v${OPENVSCODE_SERVER_VERSION}/openvscode-server-v${OPENVSCODE_SERVER_VERSION}-linux-x64.tar.gz" && \
    tar --strip-components=1 -xzf openvscode-server-v${OPENVSCODE_SERVER_VERSION}-linux-x64.tar.gz && \
    rm openvscode-server-v${OPENVSCODE_SERVER_VERSION}-linux-x64.tar.gz && \
    /vscode/bin/openvscode-server --install-extension "ms-python.python"

WORKDIR /workdir

# On préinstalle les dépendances pour que les pip installs soient rapides
RUN pip --no-cache install \
    python-dotenv \
    pypdf \
    langchain-core \
    langchain-community \
    langchain-openai \
    langchain-mistralai \
    langchain-chroma \
    chromadb \
    streamlit \
    numexpr \
    wikipedia \
    langchainhub \
    requests

# Finissons par copier tous les fichiers
COPY docker-entrypoint.sh /
COPY README.md .

# Port d'écoutes vscode
EXPOSE 3000

ENTRYPOINT ["/docker-entrypoint.sh"]
