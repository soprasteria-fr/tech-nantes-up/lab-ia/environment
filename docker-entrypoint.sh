#!/bin/sh

echo " --> Lancement de l'image Lab IA"

exec /vscode/bin/openvscode-server \
    --host 0.0.0.0 \
    --without-connection-token \
    --user-data-dir /workdir/.vscode \
    --default-folder /workdir
